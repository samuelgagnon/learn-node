// import module
const importedModule = require('./createAModule');

// import single function module
const log = require('./exportSingleFunctionModule');

// log imported module
console.log(importedModule, importedModule);

// log current module
console.log('current module', module);

// use log function form imported module
importedModule.log('i am everywhere');

// directly use function
log('i am simply everywhere');