// function private to the module
function log(message) {
  console.log(message);
}

// override export for the function
module.exports = log;