// run node inspect debugger.js
// look at global variable
// look double tab

// cont, c: Continue execution
// next, n: Step next
// step, s: Step in
// out, o: Step out
// pause: Pause running code (like pause button in Developer Tools)

// To begin watching an expression, type watch('my_expression'). The command watchers will print the active watchers. To remove a watcher, type unwatch('my_expression').

//https://nodejs.org/docs/latest-v13.x/api/debugger.html
function test(message) {
  console.log(message);
}

setInterval(test, 1000, 'I am alive');