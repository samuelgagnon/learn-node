// this is a class
const EventEmitter = require('events');

// instance of emitter class
const emitter = new EventEmitter();

// callback function
emitter.on('messageLogged', function (arg) {
  console.log('Listener called', arg);
})

// callback function with arrow function
emitter.on('messageLogged', (arg) => {
  console.log('Listener called', arg);
})

// Trigger event
emitter.emit('messageLogged', {mind: 'i have one', eye: 'i have two'});