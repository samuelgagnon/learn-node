const Logger = require('./eventInClass');
const logger = new Logger();
logger.log('the message is real');

// callback function with arrow function
logger.on('messageLogged', (arg) => {
  console.log('Listener called', arg);
})