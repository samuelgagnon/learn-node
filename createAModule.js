// log module object , is not global
// in node, every file is a module
console.log(module);


// function private to the module
function log(message) {
  console.log(message);
}

var url = 'http://machine.io/the/endpoint/of/life'

// explicity export module to make it public
module.exports.log = log;
module.exports.endpoint = url;