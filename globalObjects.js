function sayHello(name) {
  console.log('I am not' + name);
}

sayHello(' a human');

// in node you don't have the browser global object
// console.log(global);

// window doesnt exist, this will throw a runtime error
// console.log(window);


// here is the node documentation for global object (builtin) https://nodejs.org/dist/latest-v12.x/docs/api/globals.html
console.log(__dirname);
console.log(__filename);

// set timeout is a global object
setTimeout(function(){ console.log('Your world is mine'); }, 3000);

// This variable is not added to the global object
var message = 'I am alive';
console.log(global.message);
console.log(message);