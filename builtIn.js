// documentation https://nodejs.org/dist/latest-v12.x/docs/api
const path = require('path');

var pathObject = path.parse(__filename);

console.log(pathObject);

// get information for OS server, things that you could not do this with JS inside of a browser
const os = require('os');

// Template String with ECMAScript
console.log(`total memory ${os.totalmem()}`);
console.log(`free memory ${os.freemem()}`);

// File system builtin
const fs = require('fs');

const files = fs.readdirSync('./');
console.log(files);


fs.readdir('./', function (error, files) {
  if(error) console.log('error', err);
  else console.log('Result', files);
});