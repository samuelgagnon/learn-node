const http = require('http');

// create web server, this object is an event emitter
const server = http.createServer();

server.on('connection', (socket) => {
  console.log('New connection', socket);
});

server.listen(3000);

console.log('Listening on port 3000');