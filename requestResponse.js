const http = require('http');

// create web server, this object is an event emitter
const server = http.createServer((req, res) => {
  if(req.url === '/') {
    res.write('Cogito ergo sum');
    res.end();
  }

  // add more routes
  if (req.url === '/api') {
    res.write(JSON.stringify([1,2,3]));
    res.end();
  }
});

server.listen(3000);

console.log('Listening on port 3000');

// look at package.js
// look at server.js
// look at routes.js